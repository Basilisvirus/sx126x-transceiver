/*
 * BiosensorSPItest.c
 *
 * Created: 6/9/2023 12:16:32 μμ
 * Author : basil
 */ 

#include <stdio.h>
#include <stdlib.h>

#define F_CPU 16000000UL //the clock of the cpu (internal is 8 000 000 hz)
//Asynchronous normal mode (U2Xn=0)
#define BRC 1 //((F_CPU/(16*38400))-1) //define the baud rate (usually 9600)
#define DEBUGMODE 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>

#define BIT_GET(p,m) ((p) & (m)) //if(bit_get(foo, BIT(3)))
#define BIT_SET(p,m) ((p) |= (m)) //bit_set(foo, 0x01); bit_set(foo, BIT(5));
#define BIT_CLEAR(p,m) ((p) &= ~(m)) //bit_clear(foo, 0x40);
#define BIT_FLIP(p,m) ((p) ^= (m)) //bit_flip(foo, BIT(0));
#define BIT_WRITE(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) //bit_write(bit_get(foo, BIT(4)), bar, BIT(0)); (To set or clear a bit based on bit number 4:)
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))

//TX functions
void sw(const char toWrite[], uint8_t useDebugModesw); //print a string on serial
void swn(uint64_t num, int type, int ln, uint8_t useDebugModeswn); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it, uint8_t useDebugModeswf); //Take a float and print it on serial

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt);

//RX functions
void readUntill(char c_rx);

//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40];
char str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[256];
uint8_t rxBufferuint[256];
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0, rxComplete=0;
uint8_t rxProcessing = 0; //which processing array block are we
volatile uint8_t rxBufferuintFlush = 0;
/*
rxComplete is set when a receive is complete.
*/
//======================RX END=======================

//functions
void init_rxtx_function();
void ringBufferShift();//go to the next block in the buffer, if there is any

//SPI functions
void init_SPI();
//if justReadConfig is set, it just reads the configuration, and not sets the sensor.
uint8_t init_SX126(uint8_t justReadConfig);
void get_temperature();
uint8_t spiTran(uint8_t spiDataTran, uint8_t spiRead);
//SX functions
void SX126GetStatus();
void SX126SetTxParams(uint8_t power, uint8_t RampTime);
void SX126SetBufferBaseAddress(uint8_t TXBaseAddress, uint8_t RXBaseAddress);
void SX126WriteBuffer(uint8_t offset, uint8_t data[], uint8_t dataSize);
void SX126SetModulationParams(uint64_t bitrate, uint8_t pulseShape, uint8_t bandwidth, double freqDeviation);//300, 0, 25000, 0x1E
void SX126SetPacketParams(uint16_t preambleLength, uint8_t PreambleDetectorLength, uint8_t SyncWordLength, uint8_t AddrComp, uint8_t PacketType, uint8_t PayloadLength, uint8_t CRCType, uint8_t Whitening);//1, 0, 0, 0, 0, 1
void SX126SetDioIrqParams(uint16_t IrqMask, uint16_t DIO1Mask, uint16_t DIO2Mask, uint16_t DIO3Mask);
void SX126WriteRegister(uint16_t address, uint8_t data);
void SX126SetRfFrequency(uint32_t frequency);
void SX126GetPacketType(uint8_t expectedPacket);
void SX126SetTxRxTimeout(uint32_t timeout, char rxORtx);
void SX126Setup();//common functions for both RX, TX
void SX126Tx();//transmit
void SX126Rx();//receive
void SX126SetPaConfig(uint8_t PaDutyCycle, uint8_t hpMax, uint8_t deviceSel);
void SX126WaitForStatus(uint8_t waitForTxDone, uint8_t waitForRxDone);
void SX126SetTxInfinitePreamble();
void SX126SetTxContinuousWave();
void SX126setStandby(uint8_t STDBY);
void SX126ReadRegister(uint16_t address, uint8_t verifyData, uint8_t data2Verify);
void SX126GetIrqStatus();
void analyzeStatus(uint8_t statusCode);
void SX126SetPacketType(uint8_t packetType);
void SX126ReadBuffer(uint8_t offset, uint8_t numData);
void SX126Calibrate(uint8_t calibParam);
void SX126CalibrateImage(uint8_t freq1, uint8_t freq2);
void SX126SetRegulatorMode(uint8_t regModeParam);
void SX126GetDeviceErrors();
void printOK();
void WaitForBusy(uint8_t printBusyOk);
void SX126GetRxBufferStatus(uint8_t printData);
void SX126ClearIrqStatus();//clears all flags

 //============EEPROM functions
 void EEPROM_write(unsigned int uiAddress, uint8_t ucData);
 unsigned int EEPROM_read(unsigned int uiAddress, uint8_t printHex);

//temperature vars
float realTemp = 0;

//SetStandby
#define STANDBY_XOSC 1 
#define STANDBY_RC 0
//Delay
#define DELAY 10 //DELAY in ms
//Extra
#define ADDR_TXCLAMPCONFIG 0x08D8 //to fix the bug mentioned in page 105 of datasheet
//SetPacketType 0x8A
#define PACKETTYPE 0x00 //0=GFSK, 1=LORA, 3=FHSS
//SetRfFrequency 0x86
uint32_t SETRFFREQUENCY = 685000000; //430Mhz to 920Mhz. 685000000 defaut
//SetPaConfig 0x95
#define PADUTYCYCLE 4 //Power. Better keep it <=4
#define HPMAX 0x07 //No influence on SX1261
#define DEVICESEL 1 //1=SX1261 0=SX1262
//SetTxParams 0x8E
#define POWER 14 //FROM (-)239 to 14 (-17db to 14 db)
#define RAMPTIME 7 //from 0 to 7 (10us to 3400us)
//SetBufferBaseAddress 0x8F
#define TXBASEADDRESS 0
#define RXBASEADDRESS 0
//WriteBuffer 0x0E
#define DATASIZE 1 //(change payload length to be the same)
//SetModulationParams 0x8B
#define BITRATE 600UL //600b/s to 300 000b/s (choose small Bitrate and appropriate BandWidth)
#define PULSESHAPE 0 //0=NoFilt, 0x08=GAUSSBT0.3, 0x09=GAUSSBT0.5, 0x0A=GAUSSBT0.7, 0x0B=GAUSSBT1
#define BANDWIDTH 31 //9 to 31 Decimal. ~467kHz to ~4.8Khz (use lookup table, values are not linear) (choose small Bitrate and large BandWidth)
#define FDEV 50 //26214Hz
//SetPacketParams 0x8C
#define PREAMBLELENGTH 0x08//Preamble in bits, should be < than Syncword length (0x0001 to 0xFFFF)
#define PREAMBLEDETECTORLENGTH 0x04 //0=off, 4=8bits, 5=16bits, 6=24bits, 7=32bits
#define SYNCWORDLENGTH 16 //IN BITS. (0 to 64 bits Decimal)//Change in register addrress
#define ADDRCOMP 0 //0=Address filt disable, 1=activate on Node address, 2=activate on Node&broadcast//Change in register addrress
#define PACKETLENGTHKNOWN 1 //0=length of packet is known, 1=Length of packet is the 1st byte of the payload. (change DATSIZE as well)
#define PAYLOADLENGTH 1 //0 to 0xFF. (same as DATASIZE for now)
#define CRCTYPE 1 //1=off, 0=1st byte, 2=2nd byte, 4=1st byte&inverted, 6=2nd byte& inverted.
#define WHITENING 0 //0=disabled, 1=enabled. //Change in register address.
//SetTxRx 0x82/0x83
//0 = Stay in TX mode until packet transmits./ !=0 = Returns when TX done or timeout. (Duration=Timeout*15.6us)
//0 = Stay in RX mode until it receives a packet./ !=0 = Returns on Timeout or Packet reception./ 0xFFFFFF = Continuous RX Mode.
#define TIMEOUT 0x000000 //3 bytes. max timeout is 262 seconds.
//Calibrate 0x89
#define CALIBPARAM 0xFF //With CALIBPARAM=0xFF, all parameters are calibrated.
//CalibrateImage 0x98
#define FREQ1 0x6B //from 430Mhz = 0x6B
#define FREQ2 0xE9 //to 928Mhz = 0xE9
//SetRegulatorMode 0x96
#define REGMODEPARAM 0 //0=LDO, 1=DC-DC

int main(void)
{
	//_delay_ms(4000);
	//EEPROM_write(0,168);
	init_rxtx_function();
	init_SPI();//Init SPI first.
	
	//Reset pin init
	BIT_SET(DDRB, BIT(1));//PB1 SXreset output
	BIT_SET(PORTB, BIT(1));//PB1 SXreset High (no reset)
	
	//Busy pin init
	BIT_CLEAR(DDRD, BIT(2));//PD2 input Busy
	
	//LED pin PC2
	BIT_SET(DDRC, BIT(2));//PC2 output
	BIT_SET(PORTC, BIT(2));//LED on
	
	SX126Setup();
	
	//SX126SetTxContinuousWave();
	while(1)
	{
		SX126Tx();//transmit comport 87
		//SX126Rx(); //Receive port 88
	}

	readUntill('\0');
}


void SX126Setup()
{
	sw("\n\r======\n\r",1);
	//Optimize clamping
	SX126GetStatus();
	SX126WriteRegister(ADDR_TXCLAMPCONFIG, 0B00001111);//as the datasheet notes in page 105
	//1. If not in STDBY_RC mode, then go to this mode with the command SetStandby(...)
	SX126setStandby(STANDBY_XOSC);
	//change capacitor on the XTAL pins
	SX126WriteRegister(0x0911, 0x00);
	SX126WriteRegister(0x0912, 0x00);
	SX126setStandby(STANDBY_RC);
	
	//=======================RESET HERE=======================
	BIT_CLEAR(PORTB, BIT(1));//PB1 LOW, reset.
	_delay_ms(2000);//wait
	BIT_SET(PORTB, BIT(1));//PB1 High, POR
	SX126SetRegulatorMode(REGMODEPARAM);
	SX126CalibrateImage(FREQ1, FREQ2);
	SX126Calibrate(CALIBPARAM);	
	//2. Define the protocol (LoRa® or FSK) with the command SetPacketType(...)
	SX126SetPacketType(PACKETTYPE);
	SX126GetPacketType(PACKETTYPE);
	//3. Define the RF frequency with the command SetRfFrequency(...)
	SX126SetRfFrequency(SETRFFREQUENCY);//600Mhz	
	//Sync word
	SX126WriteRegister(0x06C0, 0xFF);
	SX126WriteRegister(0x06C1, 0x00);
	//SX126WriteRegister(0x06C2, 0xFF);
}

//Rx
void SX126Rx()
{
	sw("RX\n\r",1);
	//4 (as 6). Define where the data will be stored inside the data buffer in Rx with the command SetBufferBaseAddress(...)
	SX126SetBufferBaseAddress(TXBASEADDRESS,RXBASEADDRESS);
	//5 (as 8). Define the modulation parameter according to the chosen protocol with the command SetModulationParams(...)
	SX126SetModulationParams(BITRATE,PULSESHAPE,BANDWIDTH,FDEV);	
	//6 (as 9). Define the frame format to be used with the command SetPacketParams(...)
	SX126SetPacketParams(PREAMBLELENGTH,PREAMBLEDETECTORLENGTH,SYNCWORDLENGTH,ADDRCOMP,PACKETLENGTHKNOWN,PAYLOADLENGTH,CRCTYPE,WHITENING);
	//7 (as 10). Configure DIO and IRQ: use the command SetDioIrqParams(...) to select TxDone IRQ and map this IRQ to a DIO (DIO1, DIO2 or DIO3)
	SX126SetDioIrqParams(0xFFFF,0,0,0);//TX/RXdone, Preamble, Sync word
	//8 (as 11). Define Sync Word value: use the command WriteReg(...) to write the value of the register via direct register access
	
	//9. Set the circuit in reception mode: use the command SetRx(). Set the parameter to enable timeout or continuous mode
	sw("Set timeout \n\r",1);

	while (1)
	{
		SX126SetTxRxTimeout(TIMEOUT, 'r'); //0x00=single mode
		//10. Wait IRQ RxDone 2 or Timeout: Chip stays in Rx, looks for new packet if the continuous mode is selected or it will goes to STDBY_RC
		SX126WaitForStatus(0,1);
		//11. In case of the IRQ RxDone, check the status to ensure CRC is correct: use the command GetIrqStatus()
		sw("Rx done\n\r",1);
		SX126ClearIrqStatus();
		SX126GetRxBufferStatus(1);
	
		for (int i=0; i<3; i++)
		{
			BIT_CLEAR(PORTC, BIT(2));//LED off
			_delay_ms(400);
			BIT_SET(PORTC, BIT(2));//LED on
			_delay_ms(400);
		}	
	}


}

//Tx
void SX126Tx()
{
	//Transmit mode

	//4. Define the Power Amplifier configuration with the command SetPaConfig(...)
	SX126SetPaConfig(PADUTYCYCLE,HPMAX, DEVICESEL);//PAdutycycle better <=4
	//5. Define output power and ramping time with the command SetTxParams(...) (Low power PA)
	SX126SetTxParams(POWER,RAMPTIME);
	//6. Define where the data payload will be stored with the command SetBufferBaseAddress(...)
	SX126SetBufferBaseAddress(TXBASEADDRESS,RXBASEADDRESS);
	//7. Send the payload to the data buffer with the command WriteBuffer(...)
	uint8_t data2Send[DATASIZE];
	for (uint8_t loopFill=0; loopFill<DATASIZE; loopFill++)
	{
		data2Send[loopFill]=0x0A;		
	}
	SX126WriteBuffer(0,data2Send,DATASIZE);	
	SX126ReadBuffer(0,DATASIZE);
	//8. Define the modulation parameter according to the chosen protocol with the command SetModulationParams(...)
	SX126SetModulationParams(BITRATE,PULSESHAPE,BANDWIDTH,FDEV);
	//9. Define the frame format to be used with the command SetPacketParams(...)
	SX126SetPacketParams(PREAMBLELENGTH,PREAMBLEDETECTORLENGTH,SYNCWORDLENGTH,ADDRCOMP,PACKETLENGTHKNOWN,PAYLOADLENGTH,CRCTYPE,WHITENING);
	//10. Configure DIO and IRQ: use the command SetDioIrqParams(...) to select TxDone IRQ and map this IRQ to a DIO (DIO1, DIO2 or DIO3)
	SX126SetDioIrqParams(0B1000000010000000,0B1000000010000000,0,0);//TXdone, Timeout IRQ enable	
	//11. Define Sync Word value: use the command WriteReg(...) to write the value of the register via direct register access
	//12. Set the circuit in transmitter mode to start transmission with the command SetTx(). Use the parameter to enable Timeout
	//timeout disabled, send data until finish.
	
	//infinite preamble
	//SX126SetTxInfinitePreamble();
	while(1)
	{	
		BIT_SET(PORTC, BIT(2));//LED on
		SX126SetTxRxTimeout(TIMEOUT, 't');//In TX, BUSY will go low when the PA has ramped-up and transmission of preamble starts.
		//13. Wait for the IRQ TxDone or Timeout: once the packet has been sent the chip goes automatically to STDBY_RC mode
		sw("transmitting\n\r",1);

		SX126WaitForStatus(1,0);
		BIT_CLEAR(PORTC, BIT(2));//LED off
		_delay_ms(100);
	}
	
};

void SX126ClearIrqStatus()
{
	WaitForBusy(0);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	spiTran(0x02, 0);//Op code
	spiTran(0xFF, 0);//Clear High byte
	spiTran(0xFF, 0);//Clear Low byte
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
}

void SX126GetRxBufferStatus(uint8_t printData)
{
	uint8_t PayloadLengthRx, RxStartBufferPointer;
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
		
	spiTran(0x13, 0);//Op code
	spiTran(0x00, 1);//Status
	PayloadLengthRx = spiTran(0x00, 1);//payload length
	RxStartBufferPointer = spiTran(0x00, 1);//strart rx pointer
	
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS
	
	if (printData)
	{
		SX126ReadBuffer(RxStartBufferPointer, PayloadLengthRx);
	}
}

//Can be used for both transmission and reception.
uint8_t spiTran(uint8_t spiDataTran, uint8_t spiRead)
{
	static uint8_t SPDRread = 0;
	
	if ((spiRead !=0) && (spiDataTran != 0))
	{
		sw("ERROR SPI COMMAND\n\r",0);
	}

	SPDR = spiDataTran;
	
	/* Wait for reception/transmission complete, wait for interrupt flag*/
	while( BIT_GET(SPSR, BIT(7)) == 0x00 )//while SPIF bit in SPSR is 0 wait
	{
	};

	SPDRread = SPDR;
	
	if(spiRead == 1)//if we want to read the SPDR
	{
		return SPDRread;
	}
	else //do not access SPDR
	{
		return 0;
	}
};

//Wait for busy pin
void WaitForBusy(uint8_t printBusyOk)
{
	while (BIT_GET(PIND, BIT(2)) != 0x00 )//while PD2 BUSY pin is not 0, wait
	{
		//do nothing
	}	
	if (printBusyOk)
	{
		sw("Busy ok\n\r",1);	
	}

}

void printOK()
{
	sw("OK\n\r\n\r\n\r",1);	
}

void SX126GetDeviceErrors()
{
	uint8_t status, OpErrorL, OpErrorH;
	sw("GetDeviceErrors...\n\r ",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x17, 0);//Op code
	status  = spiTran(0x00, 1);//Status
	OpErrorL = spiTran(0x00, 1);//OpError
	OpErrorH = spiTran(0x00, 1);//Not used

	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS
		
	sw("status: 0x",1);
	swn(status,16,1,1);
	sw("OpErrorH ",1);
	swn(OpErrorH,16,1,1);
	sw("OpErrorL ",1);
	swn(OpErrorL,16,1,1);	
	

	printOK();	
}

void SX126SetRegulatorMode(uint8_t regModeParam)
{
	sw("SetRegulatorMode ",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	

	spiTran(0x96,0);//op code
	spiTran(regModeParam, 0);

	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS
	printOK();
}

void SX126SetPacketType(uint8_t packetType)//0,1 or 3
{
	sw("PACKET TYPE ",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	//PACKET_TYPE_GFSK 0x00 (G)FSK packet type
	//PACKET_TYPE_LORA 0x01 LoRa mode
	//PACKET_TYPE_LR_FHSS 0x03 Long Range FHSS
	
	spiTran(0x8A,0);//op code
	spiTran(packetType,0);
	
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
	printOK();
}

void SX126setStandby(uint8_t STDBY)//1 for 32Mhz crystal
{
	sw("STDBY ",1);
	WaitForBusy(1);
	
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x80,0);//op code
	spiTran(STDBY, 0);

	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
	printOK();
}

void SX126SetTxContinuousWave()
{
	sw("Wave\n\r",1);
	WaitForBusy(1);
			
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0xD1,0);//op code
	
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
	printOK();
}

void SX126SetTxInfinitePreamble()
{
	sw("infPreamble\n\r",1);
	WaitForBusy(1);
	
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0xD2,0);//op code
	
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
	printOK();
}

void SX126ReadRegister(uint16_t address, uint8_t verifyData, uint8_t data2Verify)
{
	sw("reading at address...",1);
	
	uint8_t addressH = address >> 8;  // top byte
	uint8_t addressL = address;   // Least significant byte	
	uint8_t readSPI;
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x1D, 0);//op code
	spiTran(addressH,0);
	spiTran(addressL,0);
	spiTran(0,1);//wont send anything back
	readSPI = spiTran(0,1);//read first byte
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	
	if (verifyData)//if we want to verify the data read
	{
		sw("verifying written data...",1);
		if(readSPI == data2Verify)
		{
			sw("data match\n\r",1);
		}
		else
		{
			sw("data mismatch, read data is ",1);
			swn(readSPI,16,1,1);
		}
	}
	else//if we dont want to verify the data, just print them.
	{
		sw("Data reads: ",1);
		swn(readSPI,16,1,1);
	}
	
	printOK();
}

void SX126WaitForStatus(uint8_t waitForTxDone, uint8_t waitForRxDone)
{
	static uint8_t readData, bit_helper, count=255;
	sw("wait for TX/RX to finish",1);
	
	while(waitForTxDone)//while we wait for the TX to finish
	{
		count++;
		swn(count,10,0,1);
		sw(" ",1);
		WaitForBusy(0);
		
		BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
		
		//write with op code 0xC0 GetStatus to Returns the current status of the device.
		spiTran(0xC0, 0); //get status
		readData = spiTran(0x00, 1);//receive
		
		//Set SS pin High (end transmission)
		BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS

		bit_helper = (readData & 0B01110000);//AND mask
		bit_helper = (bit_helper >> 4);
		
		if (bit_helper == 0x06)//if status shows that tx is done
		{
			waitForTxDone = 0;//tx done
		}
		
		_delay_ms(500);
	}
	
	while(waitForRxDone)
	{
		count++;
		swn(count,10,0,1);
		sw(" ",1);
		WaitForBusy(0);
		
		BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
		
		//write with op code 0xC0 GetStatus to Returns the current status of the device.
		spiTran(0xC0, 0); //get status
		readData = spiTran(0x00, 1);//receive
		
		//Set SS pin High (end transmission)
		BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS

		bit_helper = (readData & 0B01110000);//AND mask
		bit_helper = (bit_helper >> 4);
		
		if (bit_helper == 0x02)//if status shows that rx is done
		{
			waitForRxDone = 0;//rx done
		}
		
		SX126GetIrqStatus();
		_delay_ms(500);		
	}
	
	sw("TX/RX finished\n\r",1);
	count=0;
	
}


void SX126SetPaConfig(uint8_t PaDutyCycle, uint8_t hpMax, uint8_t deviceSel)//paLut = 1 always
{
	sw("PAconfig..\n\r",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	//for SX1261, setPaConfig
	spiTran(0x95,0);//op code
	spiTran(PaDutyCycle,0);//PaDutyCycle
	spiTran(hpMax,0);//hpMax between 0x00 and 0x07 (no influence on the SX1261)
	spiTran(deviceSel, 0);
	spiTran(0x01,0);//paLut always 1
	
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS
	printOK();
}

void SX126SetRfFrequency(uint32_t frequency)
{
	sw("Setting frequency..",1);
	
	if ( (frequency < 150000000) || (frequency > 960000000) )//check input
	{
		sw("ERR FREQ\n\r",1);
		frequency = 600000000;
	}
		//Define the RF frequency with the command SetRfFrequency(...) opcode: 0x86, params: rfFreq[31:0]
		
		uint64_t rfFrequency = ((uint64_t)(33554432)*(uint64_t)(frequency)) / (uint64_t)(32000000);//datasheet page 85
		sw("rfFreq ",1);
		swn(rfFrequency,16,1,1);
		
		uint8_t rfFrequencySplit[4];

		sw("rfFreqsplit ",1);
		for (int8_t freqLoop = 3; freqLoop>=0; freqLoop--)
		{
			rfFrequencySplit[freqLoop] = rfFrequency >> (freqLoop*8) ;
			swn(rfFrequencySplit[freqLoop],16,0,1);
			sw(" ",1);
		}
		sw("\n\r",1);
		sw("WaitingforBusy..\n\r",1);
		
		WaitForBusy(1);
		sw("Busy done\n\r",1);
		BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW

		spiTran(0x86, 0);//op code to send frequency.
		spiTran(rfFrequencySplit[3], 0);//
		spiTran(rfFrequencySplit[2], 0);//
		spiTran(rfFrequencySplit[1], 0);//
		spiTran(rfFrequencySplit[0], 0);//
		
		//Set SS pin High (end transmission)
		BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS	
		printOK();
}

//Define the protocol (LoRa® or FSK) with the command SetPacketType(...) its in GFSK
void SX126GetPacketType(uint8_t expectedPacket)
{
	sw("getting packet type...\n\r",1);
	uint8_t readbyte[3];
	
	WaitForBusy(1);	
				
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
				
	//write with op code 0x11 GetPacketType that Gets the current packet configuration for the device
	spiTran(0x11, 0); //send address to write
	readbyte[0] = spiTran(0x00, 1);//receive Status
	readbyte[1] = spiTran(0x00, 1);//receive packetType

	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS
				
	readbyte[0] = (readbyte[0] & 0B01111110);
	sw("status: ",1);
	swn(readbyte[0],16,1,1);
	sw("packet type: ",1);

	if (readbyte[1] == 0x00)
	{
		sw(" GFSK",1);
	}
	else if (readbyte[1] == 0x01)
	{
		sw(" LORA",1);
	}
	else if (readbyte[1] == 0x03)
	{
		sw(" LR_FHSS",1);
	}
	
	if (expectedPacket == readbyte[1])
	{
		sw(" OK",1);
	}
	else
	{
		sw(" ERROR PACKET",1);
	}
	sw("\n\r",1);
	analyzeStatus(readbyte[0]);
}

void SX126GetStatus()
{
	static uint8_t readData;
	sw("GetStatus ", 1);
	WaitForBusy(1);
	
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	//write with op code 0xC0 GetStatus to Returns the current status of the device.
	spiTran(0xC0, 0); //send address to write
	readData = spiTran(0x00, 1);//receive
	//Set SS pin High (end transmission)
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS

	analyzeStatus(readData);
	
	printOK();
}
//============================EEPROM FUNCTIONS============================

//Assuming low power is selected (with the command SetPaConfig and the parameter deviceSel=1=SX1261)
void SX126SetTxParams(uint8_t power, uint8_t RampTime)
{
	sw("SetTxParams...",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	spiTran(0x8E,0);//Op code SetTxParams
	spiTran(power,0);// 239 <= power <= 14  (-17db to 14db)
	spiTran(RampTime,0);// 0 <= RampTime <= 7 (10us to 3400us)
	
	BIT_SET(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	printOK();
}

//Store TX and RX base address in register of selected protocol modem.
void SX126SetBufferBaseAddress(uint8_t TXBaseAddress, uint8_t RXBaseAddress)
{
	sw("SetBufferBaseAddress ",1);
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x8F,0);//op code
	spiTran(TXBaseAddress, 0);
	spiTran(RXBaseAddress, 0);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
	
//7.3 Data Buffer in Transmit Mode
//Upon each transition to transmit mode TxDataPointer is initialized to TxBaseAddr and is incremented each time a byte is sent
//over the air. This operation stops once the number of bytes sent equals the payloadlength parameter as defined in the
//function SetPacketParams(...).

}

void SX126CalibrateImage(uint8_t freq1, uint8_t freq2)
{
	sw("CalibImage \n\r",1);
		
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x98,0);
	spiTran(freq1,0);
	spiTran(freq2,0);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}

//make calibParam 0xFF to enable by default all calibrations
void SX126Calibrate(uint8_t calibParam)
{
	sw("Calib \n\r",1);
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	spiTran(0x89,0);//op code
	spiTran(calibParam,0);//calibParam

	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();	
}

//store data payload to be transmitted. The address is auto-incremented; when it exceeds the value
//of 255 it is wrapped back to 0 due to the circular nature of the data buffer. The address starts with an offset set as a
//parameter of the function
void SX126WriteBuffer(uint8_t offset, uint8_t *data, uint8_t dataSize)
{
	sw("SetBufferBaseAddr\n\r",1);
	static uint8_t dataLoop;
	 
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x0E,0);//op code
	spiTran(offset,0);
	for (dataLoop=0; dataLoop<dataSize; dataLoop++)
	{
		swn(data[dataLoop],16,1,1);
		spiTran(data[dataLoop],0);
	}
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}
	
void SX126ReadBuffer(uint8_t offset, uint8_t numData)
{
	sw("ReadBuffer ",1);
	uint8_t data;
	uint8_t maxAddress = offset + numData;
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	spiTran(0x1E,0);
	spiTran(offset,0);
	spiTran(0,1);//nop
	
	for (numData=offset; numData<maxAddress; numData++)
	{
		data = spiTran(0x00,1);//data
		sw("pos ",1);
		swn(numData,10,0,1);
		sw(": ",1);
		swn(data,10,1,1);			
	}
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}

void SX126SetModulationParams(uint64_t bitrate, uint8_t pulseShape, uint8_t bandwidth, double freqDeviation)//300, 0, 25000, 0x1E
{
	sw("SetModParams ",1);
	//check bitrate
	if(bitrate < 600)
	{
		sw("bitrate <600b/s. Fixing..",1);
		bitrate = 600;
	}
	else if (bitrate > 300000)
	{
		sw("bit rate>300kb/s, fixing..",1);
		bitrate = 300000;
	}

	
	uint32_t actualBitrate = (uint32_t)(1024000000UL)/bitrate;
    // Split the lower 3 bytes into separate variables
    uint8_t actualBitrateH = actualBitrate >> 16; // Most significant byte
    uint8_t actualBitrateM = actualBitrate >> 8;  // Middle byte
    uint8_t actualBitrateL = actualBitrate;         // Least significant byte	
/*	
	sw("BitRate actualByte ",1);
	swn(actualBitrate,16,1,1);
	
	sw("byte1 ",1);
	swn(actualBitrateH,16,1,1);
	
	sw("byte2 ",1);
	swn(actualBitrateM,16,1,1);
	
	sw("byte3 ",1);
	swn(actualBitrateL,16,1,1);
*/
	
	//check PulseShape (0, 8, 9, 0x0A, 0x0B)
	if ( (pulseShape != 0x00) && (pulseShape != 0x08) && (pulseShape != 0x09) && (pulseShape != 0x0A) && (pulseShape != 0x0B) )
	{
		sw("PulseShape out of range!",1);
		swn(pulseShape, 16,0,1);
		sw(" applying default: No filter.\n\r",1);
		pulseShape = 0x00;
	}

	//check bandwith
	if (bandwidth<9)
	{
		sw("LOW BW\n\r",1);
		bandwidth = 9;
	}
	else if (bandwidth>31)
	{
		sw("HIGH BW\n\r",1);
		bandwidth = 31;
	}
	
	//make freqDev
	uint32_t fDev = ( freqDeviation * (long double)(1.048576) );//usually ~freqDeviation=25000 (page 85 datasheet)
    // Split the lower 3 bytes into separate variables
    uint8_t actualfDev1 = fDev >> 16; // Most significant byte
    uint8_t actualfDev2 = fDev >> 8;  // Middle byte
    uint8_t actualfDev3 = fDev;       // Least significant byte
/*
	sw("fDev actualByte ",1);
	swn(fDev,16,1,1);
	
	sw("byte1 ",1);
	swn(actualfDev1,16,1,1);
	
	sw("byte2 ",1);
	swn(actualfDev2,16,1,1);
	
	sw("byte3 ",1);
	swn(actualfDev3,16,1,1);
*/
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW	
	
	spiTran(0x8B,0);//opcode
	
	//GFSK ModParam1, ModParam2 & ModParam3 - br
	spiTran(actualBitrateH,0);
	spiTran(actualBitrateM,0);
	spiTran(actualBitrateL,0);
	//GFSK ModParam4 - PulseShape
	spiTran(pulseShape,0);
	//GFSK ModParam5 - Bandwidth
	spiTran(bandwidth,0);//9.7kHz = 0x1E.
	//GFSK ModParam6, ModParam7 & ModParam8 - Fdev
	spiTran(actualfDev1,0);
	spiTran(actualfDev2,0);
	spiTran(actualfDev3,0);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}

//PreambleDetectorLength must be smaller than the size of the following Sync Word
//use a minimum of 16 bits for the preamble
void SX126SetPacketParams(uint16_t preambleLength, uint8_t PreambleDetectorLength, uint8_t SyncWordLength, uint8_t AddrComp, uint8_t PacketType, uint8_t PayloadLength, uint8_t CRCType, uint8_t Whitening)
{
	sw("PACKETParams ",1);
	//preamble length
	uint8_t preambleLengthH = preambleLength >> 8;  // top byte
	uint8_t preambleLengthL = preambleLength;      // Least significant byte

	//preamble detector
	if(																  
	(PreambleDetectorLength != 0x00) && 
	(PreambleDetectorLength != 0x04) && 
	(PreambleDetectorLength != 0x05) &&
	(PreambleDetectorLength != 0x06) &&
	(PreambleDetectorLength != 0x07)
	)
	{
		sw("preamblelength ERR\n\r",1);
		PreambleDetectorLength = 0;
	} 
	
	//syncWOrdLength
	if (SyncWordLength>0x40)
	{
		sw("LONG SYNC WORD\n\r",1);
		SyncWordLength = 40;
	}
	  
	//AddrComp
	if 
	(
	(AddrComp != 0x00) && 
	(AddrComp != 0x01) &&
	(AddrComp != 0x02)	  
	)
	{
		sw("AddrComp ERR\n\r",1);
		AddrComp = 0;
	}
	
	//PacketType
	if 
	( (PacketType != 0x00) && (PacketType != 0x01)  )
	{
		sw("packet type ERR\n\r",1);
		PacketType = 0;
	}
	
	if (
	(CRCType != 0x01) &&
	(CRCType != 0x00) &&
	(CRCType != 0x02) &&
	(CRCType != 0x04) &&
	(CRCType != 0x06)
	)
	{
		sw("CRCType ERR\n\r",1);
		CRCType = 0x01;
	}
	
	if 
	(
	(Whitening != 0x00) &&
	(Whitening != 0x01)
	)
	{
		sw("Whitening ERROR\n\r",1);
		Whitening = 0x00;
	}

	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
		
	spiTran(0x8C,0);//op code
	//preamble length from 1 to 0xFFFF.
	spiTran(preambleLengthH,0);
	spiTran(preambleLengthL,0);
	
	spiTran(PreambleDetectorLength,0);	//preamble detector length 0 is off.
	spiTran(SyncWordLength, 0);	//syncWord Length
	spiTran(AddrComp,0);//AddrComp
	spiTran(PacketType,0);//packetType
	spiTran(PayloadLength, 0);//payload size
	spiTran(CRCType, 0);//CRC select
	spiTran(Whitening, 0); //Whitening

	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}

void SX126SetDioIrqParams(uint16_t IrqMask, uint16_t DIO1Mask, uint16_t DIO2Mask, uint16_t DIO3Mask)
{
	sw("Setting IRQ ",1);
	uint8_t IrqMaskH = IrqMask >> 8;  // top byte
	uint8_t IrqMaskL = IrqMask;         // Least significant byte
	
	uint8_t DIO1MaskH = DIO1Mask >> 8;  // top byte
	uint8_t DIO1MaskL = DIO1Mask;         // Least significant byte	
	
	uint8_t DIO2MaskH = DIO2Mask >> 8;  // top byte
	uint8_t DIO2MaskL = DIO2Mask;         // Least significant byte

	uint8_t DIO3MaskH = DIO3Mask >> 8;  // top byte
	uint8_t DIO3MaskL = DIO3Mask;         // Least significant byte		

	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x08,0);//op code

	spiTran(IrqMaskH,0);
	spiTran(IrqMaskL,0);		
	
	spiTran(DIO1MaskH,0);
	spiTran(DIO1MaskL,0);
	
	spiTran(DIO2MaskH,0);
	spiTran(DIO2MaskL,0);
	
	spiTran(DIO3MaskH,0);
	spiTran(DIO3MaskL,0);		

	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH	
	printOK();		
}

void analyzeStatus(uint8_t statusCode)
{
	sw("analyzeStatus..",1);
	static uint8_t bit_helper=0;
	
	statusCode = (statusCode & 0B01111110);//mask unused bits
	sw("0x",1);
	swn(statusCode,16,0,1);

	bit_helper = (statusCode & 0B01110000);//AND mask
	bit_helper = (bit_helper >> 4);

	if (bit_helper == 0x00)
	{
		sw(" Unused mode",1);
	}
	else if (bit_helper == 0x01)//Reserved for Future Use
	{
		sw(" RFU mode",1);
	}
	else if (bit_helper == 0x02)//STBY_RC mode (Top regulator (LDO), RC13M oscillator)
	{
		sw(" STBY_RC mode",1);
	}
	else if (bit_helper == 0x03)//STBY_XOSC (Top regulator (DC-DC or LDO), XOSC)
	{
		sw(" STBY_XOSC mode",1);
	}
	else if (bit_helper == 0x04)//FS mode (All of the above + Frequency synthesizer at Tx frequency)
	{
		sw(" FS mode",1);
	}
	else if (bit_helper == 0x05)//RX mode (Frequency synthesizer and receiver, Modem)
	{
		sw(" RX mode",1);
	}
	else if (bit_helper == 0x06)//TX mode (Frequency synthesizer and transmitter, Modem)
	{
		sw(" TX mode",1);
	}
	else
	{
		sw("Status bytes 6:4 ",1);
		swn(bit_helper,2,0,1);
		sw(" UNKNOWN MODE",1);
	}
	sw(", ",1);

	bit_helper = (statusCode & 0B00001110);//AND mask)
	bit_helper = (bit_helper >> 1);

	if (bit_helper == 0x00)
	{
		sw("Reserved mode",1);
	}
	else if (bit_helper == 0x01)//Reserved for Future Use
	{
		sw("RFU ",1);
	}
	else if (bit_helper == 0x02)//A packet has been successfully received and data can be retrieved
	{
		sw("Data available",1);
	}
	else if (bit_helper == 0x03)//A transaction from host took too long to complete and triggered an internal watchdog.
	{
		sw("timeout",1);
	}
	else if (bit_helper == 0x04)//Processor was unable to process command either because of an invalid opcode or because an incorrect number of parameters has been provided.
	{
		sw("ProcessingError",1);
	}
	else if (bit_helper == 0x05)//Command successfully processed but could not execute the command. For ex: it was unable to enter the specified device mode
	{
		sw("executeFail",1);
	}
	else if (bit_helper == 0x06)//The transmission of the current packet has terminated
	{
		sw("TX done",1);
	}
	else
	{
		sw("Status bytes 3:1 ",1);
		swn(bit_helper,2,0,1);
		sw(" UNKNOWN MODE",1);
	}
	printOK();
}

void SX126GetIrqStatus()
{
	sw("SX126GetIrqStatus ",1);
	static uint8_t Status, IrqStatusH, IrqStatusL;
	
	WaitForBusy(0);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x12,0);
	Status = spiTran(0,1);
	IrqStatusH = spiTran(0,1);
	IrqStatusL = spiTran(0,1);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	Status = (Status & 0B01111110);//mask unused bits
	sw("Status:",1);
	swn(Status,16,0,1);
	sw(". IrqStatus ",1);
	swn(IrqStatusH,16,0,1);
	sw(" ",1);
	swn(IrqStatusL,16,1,1);

	analyzeStatus(Status);
	
	if(BIT_GET(IrqStatusL,BIT(0)))//TxDone
	{
		sw("TxDone ",1);
	}

	if (BIT_GET(IrqStatusL,BIT(2)))//PreambleDetected
	{
		sw("PreambleDetected ",1);
	}

	if (BIT_GET(IrqStatusL,BIT(3)))//SyncWordValid
	{
		sw("SyncWordValid ",1);
	}	
	
	if (BIT_GET(IrqStatusL,BIT(1)))//RxDone
	{
		sw("RxDone ",1);
	}	
}

void SX126WriteRegister(uint16_t address, uint8_t data)
{
	sw("writing Register ",1);
	
	uint8_t addressH = address >> 8;  // top byte
	uint8_t addressL = address;         // Least significant byte	
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	spiTran(0x0D, 0);//op code
	spiTran(addressH,0);
	spiTran(addressL,0);
	spiTran(data,0);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	
	//verify data
	SX126ReadRegister(address,1,data);
	printOK();
}

void SX126SetTxRxTimeout(uint32_t timeout, char rxORtx)
{
	if (rxORtx == 'r')
	{
		sw("SetRx ",1);				
	}
	else if (rxORtx == 't')
	{
		sw("SetTx ",1);		
	}
	else
	{
		sw("ERRORTimeout\n\r",1);
	}

	
	uint8_t timeoutH = timeout >> 16;  // High byte	
	uint8_t timeoutM = timeout >> 8;  // mid byte
	uint8_t timeoutL = timeout;         // Least significant byte
	
	WaitForBusy(1);
	BIT_CLEAR(PORTB, BIT(2));//PB2=0 CS/NSS LOW
	
	if (rxORtx == 'r')
	{
		spiTran(0x82,0);//Rx
	}
	else if (rxORtx == 't')
	{
		spiTran(0x83,0);//Tx
	}
	
	spiTran(timeoutH,0);
	spiTran(timeoutM,0);
	spiTran(timeoutL,0);
	
	BIT_SET(PORTB, BIT(2));//PB2=1 CS/NSS HIGH
	printOK();
}

//inits the SPI as master, changing the default pin overrides.
void init_SPI()
{
	BIT_SET(SREG, BIT(7));//I bit in S reg set
	//=====SPI SETUP FOR PT100======
	//SS OUTPUT AND HIGH (Sensor 1)
	BIT_SET(DDRB, BIT(2));//PB2=1
	BIT_SET(PORTB, BIT(2));//PB2=1
		
	//===SPI pins override====
	//MOSI OUTPUT:
	BIT_SET(DDRB, BIT(3));//PB3=1
	//SCK OUTPUT
	BIT_SET(DDRB, BIT(5));//PB5=1
	
	//MISO SHOULD BE PULLED DOWN BY AN EXTERNAL RESISTOR
	
	//======DATA MODE====
	//Modes 0 is supported.
	//Select mode 0(CPOL=0, CPHA=0)
	BIT_CLEAR(SPCR, BIT(3));//CPOL=0
	BIT_CLEAR(SPCR, BIT(2));//CPHA=0

	
	//====DATA ORDER===
	//MSB first
	BIT_CLEAR(SPCR, BIT(5));//DORD=0

	
	//=====FUNCTION MODE SELECT=====
	//Master mode enable
	BIT_SET(SPCR, BIT(4));//MSTR=1
		
	//=====CLOCK RATE SELECT=====
	//Fosc/128
	BIT_CLEAR(SPSR, BIT(0));//SPI2X=0
	BIT_SET(SPCR, BIT(1));//SPR1=1
	BIT_SET(SPCR, BIT(0));//SPR0=1
		
	//===SPI interrupt DISABLE===
	/*
	Enabling this bit enables interrupt and interrupt routine runs when each transmission/reception is done.
	But if another interrupt runs (in this case timer 1), and we need to use SPI inside interrupt1, we
	wont be able to do so, since once timer1 interrupt runs, all other interrupts are disabled.
	*/
	BIT_CLEAR(SPCR, BIT(7));//SPIE=0
	
	//===SPI ENABLE===
	BIT_SET(SPCR, BIT(6));//SPE=1
	
	/*
	The address byte is always the first byte transferred after CS is driven low.
	The first MSB (A7) of this byte determines whether the
	following byte is written or read. If A7 is 0, one or more
	byte reads follow the address byte. If A7 is 1, one or more
	byte writes follow the address byte.
	*/	
};

//0<=Address<=1023
void EEPROM_write(unsigned int uiAddress, uint8_t ucData)
{
	/* Wait for completion of previous write/read */
	while(EECR & (1<<EEPE));
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
};

unsigned int EEPROM_read(unsigned int uiAddress, uint8_t printHex)
{
	uint8_t eeprom_ret;
	/* Wait for completion of previous write/read */
	while(EECR & (1<<EEPE));
	/* Set up address Register */
	EEAR = uiAddress;

	/* Start eeprom read by setting EEPE */
	EECR |= (1<<EERE);
	
	while(EECR & (1<<EEPE));//wait for the read to finish
	
	eeprom_ret = EEDR;
	
	if(printHex)
	{
		swh(" ", eeprom_ret, 1);
	}
	
	return eeprom_ret;
};

void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H =(BRC >> 8);//(BRC >> 8); //Put BRC to UBRR0H and move it right 8 BITs.
	UBRR0L = BRC; //BRC;
	BIT_SET(UCSR0A, BIT(1));//U2Xn = 1//double speed
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
};

void sw(const char toWrite[], uint8_t useDebugModesw)
{
	if(useDebugModesw)//if we want to use debug mode
	{
		if(DEBUGMODE)//if the debug mode is active
		{
			len = strlen(toWrite); //take size of characters to be printed
			k=0;	//initialize k
			//UDR0=0; //make sure UDR0 is 0
			
			while (k<len) //while i is less than the total length of the characters to be printed
			{
				if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
					UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
					k++;		//increase the position, and wait until UDRE0 is 1 again
				}
			}
			//udr0 = '\0';
		}
		else//if the debug mode is inactive
		{
			//do nothing
		}
	}
	else//if we do not care about debug mode, print
	{
		len = strlen(toWrite); //take size of characters to be printed
		k=0;	//initialize k
		//UDR0=0; //make sure UDR0 is 0
		
		while (k<len) //while i is less than the total length of the characters to be printed
		{
			if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
				UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
				k++;		//increase the position, and wait until UDRE0 is 1 again
			}
		}
		//udr0 = '\0';
	}
};

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt)
{	
	
	while (  ((UCSR0A & 0B00100000) != 0B00100000)  )//if UDRE0 is 1 (aka UDR0 is ready to send)
	{
		//Wait
	}

	//if we want to send a uint_8t
	if(useInt)
	{
		UDR0 = toWriteInt;
	}
	else
	{
		UDR0 = strtol(toWriteChar, 0, 16); //Transform char to hex and put the next character to be sent
	}
};

void swn(uint64_t num, int type, int ln, uint8_t useDebugModeswn) //take an int and print it on serial
{
	static char str_intx[50];//declare a string of 50 characters
	
	ltoa(num, str_intx, type);//convert from long to char and save it on str
	
	sw(str_intx, useDebugModeswn); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r",useDebugModeswn);
	}
};

//Input: float number, output: print float number to serial, OR only return the float as string.
//Take a float and print it on serial
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it, uint8_t useDebugModeswf) 
{
	
	if(extra_accur == 0 || extra_accur == 1)//works with very good accuracy
	{
		/*
		* Needs library linking. Below tutorial is for Microchip studio.
		* Project properties toolchain..XC8 Linker/General -> Tick 'Use vprintf library(-Wl,-u,vprintf)
		* And: XC8 Linker/Miscallaneous insert at 'Other linker flags' this: -lprintf_flt
		* https://startingelectronics.org/articles/atmel-AVR-8-bit/print-float-atmel-studio-7/
		*/
		sprintf(str_floatx, "%f", numf); //needs library linking or itll print something like ' ?); '
	}

	if(print_it)
	{
		sw(str_floatx,useDebugModeswf);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n",useDebugModeswf);
		}
	}
};

//==================================TX END=====================================


//the result (input) is stored in: char rxBuffer[]
void readUntill(char c_rx)
{
	rxBuffer[0] = '\0';
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
};

ISR(USART_RX_vect)
{
	if(readString == 1)//if we are using readUntill
	{		
		rxBufferuint[rxWritePos] = UDR0;//save incoming char
		
		/*
		rxBuffer[rxWritePos] = UDR0;//save incoming char
		rxBuffer[rxWritePos+1] = '\0' ;//end char
		
		
		if( (rxBuffer[rxWritePos] == rx_stop) && (pauseReadingRx == 1) )
		{//if we meet the end char AND the Pause reading Rx is active
			readString = 0;
			//when you initialize a character array by listing all of its characters 
			//separately then you must supply the '\0' character explicitly
			rxBuffer[rxWritePos] = '\0' ;
		}
		*/
		
		rxWritePos++;
		
		if(rxWritePos == 0)
		{//if we are back at 0
			rxBufferuintFlush = 1;//buffer flushed and overflowed
		}
		
		rxComplete = 0B00000001;
	}
	else
	{
		udr0 = UDR0;	//udr0 variable can be used to store the input from the user.
		rxComplete = 0B00000001;//flag for receive complete.
	}
};

//==================================RX END==================================

/*
Shifts the buffer pointer to the next array block, if there is any available next block. This function does not pause the
Code. If we are waiting for an incoming package to arrive, we should run this function inside a loop, by storing the rxProcessing
value to another variable before the loop, and wait untill this variable changes. This will mean that the buffer moved aka
a new package arrived.
*/
void ringBufferShift()
{
	if( (rxProcessing < (rxWritePos-(1))) || ( (rxBufferuintFlush == 1U) && ( (uint8_t)(rxProcessing + 1U) != ((uint8_t)rxWritePos) ) ) )
	{//load the next block of the buffer
		
		++rxProcessing;
		
		if(rxProcessing == 0)
		{//if we are back from where we started
			rxBufferuintFlush = 0;//reset the flush flag
		}
	}
};