\contentsline {section}{\numberline {1}Acknowledgments}{3}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{4}{section.2}%
\contentsline {section}{\numberline {3}About this document and the project}{4}{section.3}%
\contentsline {section}{\numberline {4}PCB stackup}{4}{section.4}%
\contentsline {section}{\numberline {5}resistance vs reactance}{4}{section.5}%
\contentsline {section}{\numberline {6}displacement current vs return current}{4}{section.6}%
\contentsline {section}{\numberline {7}The use of 2D/3D solvers}{4}{section.7}%
\contentsline {section}{\numberline {8}About signal propagation}{4}{section.8}%
\contentsline {subsection}{\numberline {8.1}The path of the return currents}{6}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Definition of AC signal and Frequency domain}{7}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Furmula for calculating the frequency based on rise time}{10}{subsection.8.3}%
\contentsline {subsection}{\numberline {8.4}Localization of Signal Pulse (AC) Energy on a Microstrip PCB}{10}{subsection.8.4}%
\contentsline {subsection}{\numberline {8.5}A cut/gap on the reference plane under signal traces}{11}{subsection.8.5}%
\contentsline {subsection}{\numberline {8.6}The role of signal/trace impedance}{11}{subsection.8.6}%
\contentsline {subsection}{\numberline {8.7}Non-ideal Capacitors, Inductors}{16}{subsection.8.7}%
\contentsline {subsection}{\numberline {8.8}Attenuation due to PCB substrate}{23}{subsection.8.8}%
\contentsline {subsection}{\numberline {8.9}Transmitting signals through air medium}{24}{subsection.8.9}%
\contentsline {section}{\numberline {9}Antenna type selection}{25}{section.9}%
\contentsline {subsection}{\numberline {9.1}Antenna simulation}{26}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Antenna parameters}{28}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Physical Antenna characterization}{29}{subsection.9.3}%
\contentsline {subsubsection}{\numberline {9.3.1}NanoVNA for characterization}{30}{subsubsection.9.3.1}%
\contentsline {subsubsection}{\numberline {9.3.2}tinyVNA for impedance matching}{35}{subsubsection.9.3.2}%
\contentsline {subsubsection}{\numberline {9.3.3}tinySA ULTRA for characterization}{35}{subsubsection.9.3.3}%
\contentsline {subsection}{\numberline {9.4}Antenna return path}{36}{subsection.9.4}%
\contentsline {subsection}{\numberline {9.5}Antenna impedance matching}{37}{subsection.9.5}%
\contentsline {section}{\numberline {10}Microcontroller}{37}{section.10}%
\contentsline {subsection}{\numberline {10.1}Capabilities and features}{38}{subsection.10.1}%
\contentsline {section}{\numberline {11}Tranceiver}{39}{section.11}%
\contentsline {subsection}{\numberline {11.1}Capabilities}{41}{subsection.11.1}%
\contentsline {subsection}{\numberline {11.2}Datasheet strip}{41}{subsection.11.2}%
\contentsline {section}{\numberline {12}Firmware}{41}{section.12}%
\contentsline {section}{\numberline {13}Troubleshooting and mistakes during development}{41}{section.13}%
\contentsline {section}{\numberline {14}Future improvements suggestions}{41}{section.14}%
\contentsline {section}{\numberline {15}Code sources}{42}{section.15}%
\contentsline {section}{\numberline {16}Abbreviations}{45}{section.16}%
