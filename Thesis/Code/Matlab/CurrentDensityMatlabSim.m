mytrace = traceLine;
mytrace.Width = 0.0007;
mytrace.Corner = "smooth";
slen = 0.009;
mlen = slen*2
mytrace.Length = [slen mlen slen];
mytrace.Angle  = [0 270 180];
pcb = pcbComponent;
d = dielectric('FR4');
gnd = traceRectangular('Length', slen*3,'Width',mlen*2,'Center',[slen/2,-mlen/2]);
d.Thickness = 0.0009;
pcb.BoardThickness = d.Thickness;
pcb.Layers = {mytrace,d,gnd};
pcb.BoardShape = gnd;
pcb.FeedDiameter = mytrace.Width;
pcb.FeedLocations = [0,0,1,3;0,-mlen,1,3];

figure;
show(pcb)
figure;
current(pcb,10000);%10kHz
figure;
current(pcb,500e6);%500MHz
figure;
current(pcb,1e9);%1GHz