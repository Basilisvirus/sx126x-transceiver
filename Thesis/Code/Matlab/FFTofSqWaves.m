% Parameters
% Frequency of the square wave in Hz
f = 60; 
duty_cycle = 100/2 % Duty cycle in percentage
% Duration for which the wave is generated (in seconds)
duration = 0.3; 
fs = 10000; % Sampling frequency in Hz (increased)
rise_time = 0.0000; % Rise time in seconds 0.0030
A = 5; % Desired A range from 0 to 5

% Time vector
t = 0:1/fs:duration;

% Generate square wave
square_wave = (A/2)*square(2*pi*f*t, duty_cycle);
square_wave2 = A/2+(A/2)*square(2*pi*f*t, duty_cycle);

% Create a custom square wave with specified rise time
csw = zeros(size(t));
csw2 = zeros(size(t));
% Number of samples for the rise time
trSmpls = round(rise_time * fs);

for i = 1:length(t)
    if square_wave(i) > 0
       csw(i)=min(A/2,csw(max(i-1,1))+(A/2)/trSmpls);
    else
       csw(i)=max(-A/2,csw(max(i-1,1))-(A/2)/trSmpls);
    end
end

for i = 1:length(t)
    if square_wave2(i) > 0
        csw2(i)=min(A,csw2(max(i-1,1))+(A/2)/trSmpls);
    else
        csw2(i)=max(0,csw2(max(i-1,1))-(A/2)/trSmpls);
    end
end

% Plot the square wave
figure;

% First subplot
% Create a 2x1 grid of subplots, use the first
subplot(2, 2, 1); 
% Set the line width
plot(t, csw, 'LineWidth', 1); 
% Change font size of grid numbers
set(gca, 'FontSize', 18); 
%xlabel('Seconds', 'FontSize', 19);
ylabel('Amplitude', 'FontSize', 19);
xlabel('Seconds', 'FontSize', 19);
title('Square wave signal T=1/60s');
ylim([-3 3]); % Set y-axis limits
grid on;
yticks ([-2.5 ,0 ,2.5]);

% Second subplot
subplot(2, 2, 2); % Use the second subplot
% Plot the same square wave for illustration
plot(t, csw2, 'LineWidth', 1); 
% Change font size of grid numbers
set(gca, 'FontSize', 18); 
xlabel('Seconds', 'FontSize', 19);
ylabel('Amplitude', 'FontSize', 19);
title('Square wave, spans on positive axis T=1/60s');
ylim([-0.5 5.5]); % Set y-axis limits
grid on;
yticks ([0 ,2.5, 5]);

% Define the length for zero-padding (higher value for higher resolution)
% Example: four times the next power of 2
nfft1 = 10000 * 2^nextpow2(length(csw));
nfft2 = 10000 * 2^nextpow2(length(csw2));

% Calculate FFT of the first square wave
Y1 = fft(csw,nfft1);
L1 = length(csw);
P2_1 = abs(Y1/L1);
P1_1 = P2_1(1:nfft1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);

% Calculate FFT of the second square wave
Y2 = fft(csw2,nfft2);
L2 = length(csw2);
P2_2 = abs(Y2/L2);
P1_2 = P2_2(1:nfft2/2+1);
P1_2(2:end-1) = 2*P1_2(2:end-1);

% Frequency vector
Fs = 1/(t(2)-t(1)); % Sampling frequency
f1 = Fs*(0:(nfft1/2))/nfft1;
f2 = Fs*(0:(nfft2/2))/nfft2;

% Find the indices where frequency<=330Hz
max_freq = 2000;
idx1 = find(f1 <= max_freq);
idx2 = find(f2 <= max_freq);

% Plot the FFT results up to 1500 Hz
subplot(2,2,3)
plot(f1(idx1), P1_1(idx1))
set(gca, 'FontSize', 18); 
Ax = gca;
set(Ax.XAxis, 'TickValues', 0:60:700);
title('FFT of square wave signal')
xlabel('f (Hz)')
ylabel('|P1(f)|')
grid on;
xlim([0 700]); % Set x-axis limits
ylim([0 3.1]); % Set y-axis limits

%zoom xon;

subplot(2,2,4)
plot(f2(idx2), P1_2(idx2))
set(gca, 'FontSize', 18); 
Ax = gca;
set(Ax.XAxis, 'TickValues', 0:60:700);
title('FFT of Square Wave with DC component')
xlabel('f (Hz)', 'FontSize', 19)
ylabel('|P1(f)|', 'FontSize', 19)
grid on;
xlim([0 700]); % Set x-axis limits
ylim([0 3.3]); % Set y-axis limits

zoom xon;
