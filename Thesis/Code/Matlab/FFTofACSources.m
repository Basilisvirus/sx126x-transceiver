%%Time specifications:
Fs = 100e3;% samples per second
Ts = 1/Fs;% seconds per sample
Ls = 100e3; % Length of signal
t = (0:Ls)*Ts; % Time vector
%%Sine wave:
Fc = 60;% hertz
r1 = 2.5*cos(2*pi*Fc*t);
r2 = 2.5+ 2.5*cos(2*pi*Fc*t);

tiledlayout(2,2)

% Top plot
nexttile
plot(t,r1,'r');
% Change font size
set(gca, 'FontSize', 18);
xlabel('seconds', 'FontSize', 19);
ylabel('Volts', 'FontSize', 19);
title('Sine wave 60hz', 'FontSize', 19);
grid on
% Set the maximum span of axes
ylim([-2.5, 2.5]); 
yticks([-2.5,0,2.5]);
%zoom xon;

% Bottom plot
nexttile  
plot(t,r2,'r');
set(gca, 'FontSize', 18);
xlabel('seconds', 'FontSize', 19);
ylabel('Volts', 'FontSize', 19);
title('Sine wave 60Hz with DC component', 'FontSize', 19);
grid on
ylim([0, 5]);
yticks([0,2.5,5]);
%zoom xon;

% Defining n value for DFT operation
np = 2^nextpow2(Ls);
d = 2;
% Calling fft() for the matrix f
F = fft(r1,np,d);
PS2 = abs(F/Ls);
PS1 = PS2(:,1:np/2+1);
PS1(:,2:end-1) = 2*PS1(:,2:end-1);  

% Compute frequency axis values
frequencies = Fs*(0:(np/2))/np; 
% Find indices where frequency <= 100
indices = find(frequencies <= 200); 
% Keep only the frequencies up to 100 Hz
frequencies = frequencies(indices);
% Keep corresponding FFT values
PS1 = PS1(:,1:length(frequencies)); 
nexttile

plot(frequencies, PS1);
set(gca, 'FontSize', 18);
Ax = gca;
set(Ax.XAxis, 'TickValues', 0:60:300);
xlabel('frequency (Hz)', 'FontSize', 19);
ylabel('|fft()|', 'FontSize', 19);
title('FFT of sine wave 60 Hz', 'FontSize', 19);

np = 2^nextpow2(Ls);
d = 2;
F = fft(r2,np,d);
PS2 = abs(F/Ls);
PS1 = PS2(:,1:np/2+1);
PS1(:,2:end-1) = 2*PS1(:,2:end-1);  

frequencies = Fs*(0:(np/2))/np; 
indices = find(frequencies <= 200); 
% Keep only the frequencies up to 100 Hz
frequencies = frequencies(indices);
% Keep corresponding FFT values
PS1 = PS1(:,1:length(frequencies)); 
nexttile

plot(frequencies, PS1);
set(gca, 'FontSize', 18); 
Ax = gca;
set(Ax.XAxis, 'TickValues', 0:60:300);
xlabel('frequency (Hz)', 'FontSize', 19);
ylabel('|fft()|', 'FontSize', 19);
title('FFT of sine wave 60Hz with DC component', 'FontSize', 19);
ylim([0 2.5]); % Set y-axis limits