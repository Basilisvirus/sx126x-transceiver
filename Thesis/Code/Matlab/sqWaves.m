% Parameters
% Frequency of the square wave in Hz
f = 60; 
T = 1/f; % Period of the square wave
% Duration for which the wave is generated (in seconds)
duration = 0.06; 
fs = 10000; % Sampling frequency in Hz

% Time vector
t = 0:1/fs:duration;

% Generate square wave
square_wave = 2.5*square(2*pi*f*t);
square_wave2 = 2.5+2.5*square(2*pi*f*t);

% Plot the square wave
figure;

% First subplot
% Create a 2x1 grid of subplots, and use the first subplot
subplot(2, 1, 1); 
% Set the line width
plot(t, square_wave, 'LineWidth', 2); 
% Change font size of grid numbers
set(gca, 'FontSize', 18); 
%xlabel('Seconds', 'FontSize', 19);
ylabel('Amplitude', 'FontSize', 19);
title('Signal source, square wave signal');
ylim([-3 3]); % Set y-axis limits
grid on;
yticks ([-2.5 ,0 ,2.5]);

% Second subplot
subplot(2, 1, 2); % Use the second subplot
% Plot the same square wave for illustration
plot(t, square_wave2, 'LineWidth', 2); 
% Change font size of grid numbers
set(gca, 'FontSize', 18); 
xlabel('Seconds', 'FontSize', 19);
ylabel('Amplitude', 'FontSize', 19);
title('Square Wave signal, voltage span on positive axis');
ylim([-0.5 5.5]); % Set y-axis limits
grid on;
yticks ([0 ,2.5, 5]);