# SX126x transceiver 

My ongoing master;s degree final thesis. A open-source FSK/LoRa PCB with PCB antenna.  

In order to compile the main.pdf:  
1. Install [MiKTeX](https://miktex.org/download) 
and [TexMaker](https://www.xm1math.net/texmaker/download.html) on windows.  
2. Open TexMaker and open the files man.tex and references.bib under sx126x-transceiver\Thesis folder.  
3. On options/Configure TexMaker/Commands, change Bib(la)tex to point at biber.exe, example:
"C:/Users/Vasilis/AppData/Local/Programs/MiKTeX/miktex/bin/x64/biber.exe" %  
4. Under options/Configure TexMaker/Quick Build, Select the option "PdfLaTeX+Bib(la)tex+PdfLaTeX(x2)+View Pdf  
5. While looking at the main.tex, compile the project, the .pdf should be generated. 

To program the atmega328p:
1. Using atmel-ice with microchip studio, change the default fuses to extended=0xFC, High=0xD6, Low=0xF7.  
The MCU is Atmega328p and uses the external 16MHz oscillator.  

PCB designed with KiCad.  

![MatLab S parameter](.gitimages/1.png)  
![MatLab 3D view](.gitimages/2.png)  
![KiCad 3D view PCB](.gitimages/3.png)  
![Ordered PCB test](.gitimages/4.png)  


Antenna selection [from TI](https://www.ti.com/lit/an/swra351b/swra351b.pdf?ts=1687088411603)
[LoRa Forum](https://forum.lora-developers.semtech.com/top/quarterly)